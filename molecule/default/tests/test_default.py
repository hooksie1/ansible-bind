import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_bind_installed(host):
    bind = host.package("bind")
    assert bind.is_installed


def test_bind_running(host):
    named = host.service("named")
    assert named.is_running
    assert named.is_enabled


def test_hosts_file(host):
    file = host.file("/etc/named/")
    assert file.is_directory
